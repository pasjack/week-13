from turtle import *
from win import check_win
from tkinter_gif import draw_gif

speed(0)
draw_gif("flaming-skull.gif", -50, -50)

def board():
    color("white")
    width(5)
    penup()
    goto(-200, 200)
    pendown()

    # outside board

    forward(300)
    right(90)
    forward(300)
    right(90)
    forward(300)
    right(90)
    forward(300)

    # rows
    right(180)
    forward(100)
    left(90)
    forward(300)
    right(90)
    forward(100)
    right(90)
    forward(300)

    # cols

    right(90)
    forward(200)
    right(90)
    forward(100)
    right(90)
    forward(300)
    left(90)
    forward(100)
    left(90)
    forward(300)

    penup()
    goto(-200, 200)
    right(90)


def x():
    color("yellow")
    width(10)
    penup()

    forward(10)
    right(90)
    forward(10)
    left(45)

    pendown()
    forward(1.41 * 80)
    back(1.41 * 80)
    penup()

    left(45)
    penup()
    forward(80)
    pendown()
    right(135)
    forward(1.41 * 80)
    back(1.41 * 80)
    penup()
    left(45)
    back(10)
    left(90)
    back(90)


def o():
    color("cyan")
    width(10)
    penup()
    right(90)
    forward(90)
    left(90)
    forward(50)
    pendown()
    circle(40)

    penup()
    back(50)
    left(90)
    forward(90)
    right(90)


def goto_tictac(x, y):
    penup()
    x_offset = (100 * (x - 1))
    y_offset = (100 * (y - 1))
    forward(x_offset)
    right(90)
    forward(y_offset)
    left(90)


def reset():
    goto(-200, 200)


def swap_player(current_type):
    if current_type == "o":
        return "x"
    else:
        return "o"


def check_cords(x, y):
    x -= 1
    y -= 1
    if board_list[x][y] == -1:
        return True
    else:
        return False


board()
reset()

current_player = "x"
board_list = [[-1, -1, -1], [-1, -1, -1], [-1, -1, -1]]  # empty board

while True:
    cords = textinput(current_player, "Where to place your symbol")
    my_dict = {1 : "11", 2 : "21", 3 : "31", 4 : "12", 5 : "22", 6 : "32", 7 : "13", 8 : "23", 9 : "33"}
    x_cord = int(my_dict[int(cords)][0])
    y_cord = int(my_dict[int(cords)][1])
    if check_cords(x_cord, y_cord):
        goto_tictac(x_cord, y_cord)
        if current_player == "o":
            o()
            board_list[x_cord-1][y_cord-1] = 1
        else:
            board_list[x_cord-1][y_cord-1] = 0
            x()
        if check_win(board_list):
            break
        current_player = swap_player(current_player)
        reset()
    else:
        continue

done()
