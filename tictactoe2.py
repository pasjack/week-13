
def check_win(board_lists):
    win = False

    #top row
    if board_lists[0] == ["X","X","X"] or board_lists[0] == ["O","O","O"]:
        win = True

    #middle row
    elif board_lists[1] == ["X","X","X"] or board_lists[1] == ["O","O","O"]:
        win = True

    #bottom row
    elif board_lists[2] == ["X","X","X"] or board_lists[2] == ["O","O","O"]:
        win = True

    #left column
    elif (board_lists[0][0] == "X" and board_lists[1][0] == "X" and board_lists[2][0] == "X") or (board_lists[0][0] == "O" and board_lists[1][0] == "O" and board_lists[2][0] == "O"):
        win = True

    #middle column
    elif (board_lists[0][1] == "X" and board_lists[1][1] == "X" and board_lists[2][1] == "X") or (board_lists[0][1] == "O" and board_lists[1][1] == "O" and board_lists[2][1] == "O"):
        win = True

    #right column
    elif (board_lists[0][2] == "X" and board_lists[1][2] == "X" and board_lists[2][2] == "X") or (board_lists[0][2] == "O" and board_lists[1][2] == "O" and board_lists[2][2] == "O"):
        win = True

    #diagonal down
    elif (board_lists[0][0] == "X" and board_lists[1][1] == "X" and board_lists[2][2] == "X") or (board_lists[0][0] == "O" and board_lists[1][1] == "O" and board_lists[2][2] == "O"):
        win = True

    #diagonal up
    elif (board_lists[0][2] == "X" and board_lists[1][1] == "X" and board_lists[2][0] == "X") or (board_lists[0][2] == "O" and board_lists[1][1] == "O" and board_lists[2][0] == "O"):
        win = True

    return win

def print_board(board_lists):
    #printing board
    print()
    print(board_lists[0])
    print(board_lists[1])
    print(board_lists[2])
    print()


def game():
    board_lists = [['1','2','3'],['4','5','6'],['7','8','9']]  
    pos_list = []  

    print("X goes first")
    count = 1
    piece = "X"

    print_board(board_lists)
    for i in range(9):
        

        if count % 2 == 0:
            piece = "O"
        else:
            piece = "X"

        pos = int(input(f"\nWhat position do you want to place {piece}? (1-9) "))
        while pos in pos_list or pos < 1 or pos > 9:
            print("Error, that pos has already been chosen")
            pos = int(input(f"\nWhat position do you want to place {piece}? (1-9) "))

        pos_list.append(pos)
        
        if pos == 1:
            board_lists[0][0] = piece
        
        elif pos == 2:
            board_lists[0][1] = piece

        elif pos == 3:
            board_lists[0][2] = piece

        elif pos == 4:
            board_lists[1][0] = piece
        
        elif pos == 5:
            board_lists[1][1] = piece

        elif pos == 6:
            board_lists[1][2] = piece

        elif pos == 7:
            board_lists[2][0] = piece
        
        elif pos == 8:
            board_lists[2][1] = piece

        elif pos == 9:
            board_lists[2][2] = piece

        win = check_win(board_lists)
        if win == True:
            print()
            print_board(board_lists)
            print(piece, "Won!\n")
            result = "Win"
            return result
    
        count += 1
        print_board(board_lists)

    result = "Tie"
    return result


print("\nWelcome to Tic Tac Toe!\n")
quit = ''
while quit != 'n':
    result = game()
    if result == "Tie":
        print()
        print("It was a Tie!\n")

    quit = input("\nWould you like to play again? (y/n) ")

