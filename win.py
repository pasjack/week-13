#board_lists = [['','',''],['','',''],['','','']]


def check_win(board_lists):
    win = False

    #top row
    if board_lists[0] == [0,0,0] or board_lists[0] == [1,1,1]:
        win = True

    #middle row
    elif board_lists[1] == [0,0,0] or board_lists[1] == [1,1,1]:
        win = True

    #bottom row
    elif board_lists[2] == [0,0,0] or board_lists[2] == [1,1,1]:
        win = True

    #left column
    elif (board_lists[0][0] == 0 and board_lists[1][0] == 0 and board_lists[2][0] == 0) or (board_lists[0][0] == 1 and board_lists[1][0] == 1 and board_lists[2][0] == 1):
        win = True

    #middle column
    elif (board_lists[0][1] == 0 and board_lists[1][1] == 0 and board_lists[2][1] == 0) or (board_lists[0][1] == 1 and board_lists[1][1] == 1 and board_lists[2][1] == 1):
        win = True

    #right column
    elif (board_lists[0][2] == 0 and board_lists[1][2] == 0 and board_lists[2][2] == 0) or (board_lists[0][2] == 1 and board_lists[1][2] == 1 and board_lists[2][2] == 1):
        win = True

    #diagonal down
    elif (board_lists[0][0] == 0 and board_lists[1][1] == 0 and board_lists[2][2] == 0) or (board_lists[0][0] == 1 and board_lists[1][1] == 1 and board_lists[2][2] == 1):
        win = True

    #diagonal up
    elif (board_lists[0][2] == 0 and board_lists[1][1] == 0 and board_lists[2][0] == 0) or (board_lists[0][2] == 1 and board_lists[1][1] == 1 and board_lists[2][0] == 1):
        win = True

    return win


quit = ''
while quit != 'y':

    board_lists = [['','',''],['','',''],['','','']]
    for i in range(3):
        for j in range(3):
            piece = int(input())
            board_lists[i][j] = piece

    win = check_win(board_lists)

    if win == True:
        print("Won")

    else:
        print("not yet")

    quit = input()
